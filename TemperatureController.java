import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// The Controller coordinates interactions
// between the View and Model

public class TemperatureController {

  private ConvertView theView;
  private TemperatureModel theModel;

  public TemperatureController(ConvertView theView, TemperatureModel theModel) {
    this.theView = theView;
    this.theModel = theModel;

    // Tell the View that when ever the convert button
    // is clicked to execute the actionPerformed method
    // in the ConvertListener inner class

    this.theView.addConvertListener(new ConvertListener());
  }

  class ConvertListener implements ActionListener{

    public void actionPerformed(ActionEvent e) {

      float celsiusTemperature;

      // Surround interactions with the view with
      // a try block in case numbers weren't
      // properly entered

      try{
        celsiusTemperature = theView.getCelsiusTemperature();
        theModel.convert(celsiusTemperature);
        theView.setConvertSolution(theModel.getTempFahrenheit());

      }

      catch(NumberFormatException ex){
        System.out.println(ex);
        theView.displayErrorMessage("Please enter the tempreature in Celsius");

      }

    }

  }

}
