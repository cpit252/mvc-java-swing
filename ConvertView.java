import java.awt.event.ActionListener;

import javax.swing.*;

public class ConvertView extends JFrame{
  private JLabel celsiusLabel = new JLabel("C");
  private JLabel fahrenheitLabel = new JLabel("F");
  private JTextField celsiusTemperature  = new JTextField(10);
  private JButton convertButton = new JButton("Convert");
  private JTextField calcSolution = new JTextField(10);

  ConvertView(){
    JPanel calcPanel = new JPanel();
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setSize(600, 200);
    calcPanel.add(celsiusTemperature);
    calcPanel.add(celsiusLabel);
    calcPanel.add(convertButton);
    calcPanel.add(calcSolution);
    calcPanel.add(fahrenheitLabel);
    calcSolution.setEditable(false);
    this.add(calcPanel);
  }

  public float getCelsiusTemperature(){
    return Float.parseFloat(celsiusTemperature.getText());
  }

  public float getConvertSolution(){
    return Float.parseFloat(calcSolution.getText());
  }

  public void setConvertSolution(float solution){
    calcSolution.setText(Float.toString(solution));
  }

  void addConvertListener(ActionListener listenForConvertButton){
    convertButton.addActionListener(listenForConvertButton);
  }

  void displayErrorMessage(String errorMessage){
    JOptionPane.showMessageDialog(this, errorMessage);
  }

}
