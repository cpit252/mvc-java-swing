public class MVCConverter {

  public static void main(String[] args) {

    ConvertView theView = new ConvertView();

    TemperatureModel theModel = new TemperatureModel();

    TemperatureController theController = new TemperatureController(theView,theModel);

    theView.setVisible(true);

  }
}
