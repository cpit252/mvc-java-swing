public class TemperatureModel {

	// Holds the value of the converted tempreatured in fahrenheit
	
	private float fahrenheit;
	
	public void convert(float celsius){
		fahrenheit =((celsius*9)/5)+32;  
	}
	
	public float getTempFahrenheit(){
		return fahrenheit;
	}
	
}
